$( document ).ready(function() {
	
	// SUBMIT FORM
    $("#customerForm").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
    
    
    function ajaxPost(){
    	
    	// PREPARE FORM DATA
    	var formData = {
    		url :  $("#url").val()
    	}
    	
    	// DO POST
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : window.location + "parse",
			data : JSON.stringify(formData),
			dataType : 'json',
			success : function(result) {
				var myTable= "<table><tr><td style='width: 200px; color: red;'>Title</td>";
			    myTable+= "<td style='width: 200px; color: red; text-align: right;'>Value</td>";

			    myTable+="<tr><td style='width: 200px;                   '>---------------</td>";
			    myTable+="<td     style='width: 200px; text-align: right;'>---------------</td>";
			    
			    myTable+="<tr><td style='width: 200px;'>Title:</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.siteTitle + "</td></tr>";
			    
			    myTable+="<tr><td style='width: 200px;'>HTML version:</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.htmlVersion + "</td></tr>";
			    
			    myTable+="<tr><td style='width: 200px;'>Hypermedia Links (internal):</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.hypermediaLinks.internal + "</td></tr>";
			    myTable+="<tr><td style='width: 200px;'>Hypermedia Links (external):</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.hypermediaLinks.external + "</td></tr>";
			    
			    myTable+="<tr><td style='width: 200px;'>Headings (H1-H6):</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.headingsCounter.h1 + " - " 
			   															 + result.headingsCounter.h2 + " - "
			   															 + result.headingsCounter.h3 + " - "
			   															 + result.headingsCounter.h4 + " - "
			   															 + result.headingsCounter.h5 + " - "
			   															 + result.headingsCounter.h6 +"</td></tr>";
			    
			    myTable+="<tr><td style='width: 200px;'>Contains a login Form:</td>";
			    myTable+="<td style='width: 200px; text-align: right;'>" + result.containsLoginForm + "</td></tr>";
			    
			    myTable+="</table>";
			    $("#postResultDiv").html(myTable);
				
				console.log(result);
			},
			error : function(e) {
				$("#postResultDiv").html("<p>"+e.responseJSON.message+"</p>");
				console.log("ERROR: ", e);
			}
		});
    	
    	// Reset FormData after Posting
    	resetData();

    }
    
    function resetData(){
    	$("#url").val("");
    }
})