package org.scout24.service;

import org.scout24.exception.ParserException;
import org.scout24.model.UrlModel;

public interface IParser {
	UrlModel parseUrl(String url) throws ParserException;
}
