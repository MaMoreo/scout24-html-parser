package org.scout24.service;

import java.io.IOException;
import java.net.UnknownHostException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.scout24.exception.ParserException;
import org.scout24.model.UrlModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class HtmlParserService implements IParser {

	/**
	 * Perform the parsing of the URl.
	 * 
	 * @throws ParserException 
	 */
	public UrlModel parseUrl(String url) throws ParserException {
		Document doc = null;
		UrlModel modelo = null;
		try {
			doc = Jsoup.connect(url).get();
			modelo = new UrlModel(doc);
		} catch (UnknownHostException | IllegalArgumentException e) {
			throw new ParserException("Unknown host: " + e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (IOException  e) {
			throw new ParserException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
		return modelo;
	}
}
