package org.scout24.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class ParserException extends RuntimeException {

	private HttpStatus statusCode = null;
	
	/**
	 * There was a problem parsing the URL.
	 */
	public ParserException(String message, HttpStatus status) {
		super("Problem parsing due to '" + message + "'.");
		statusCode = status;
	}
	
	/**
	 * There was a problem parsing the URL.
	 */
	public ParserException(String message) {
		super("Problem parsing due to '" + message + "'.");
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
}
