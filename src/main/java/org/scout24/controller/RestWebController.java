package org.scout24.controller;

import org.scout24.exception.ParserException;
import org.scout24.model.UrlModel;
import org.scout24.model.UrlToParse;
import org.scout24.service.IParser;
import org.scout24.service.HtmlParserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestWebController {
	
	@PostMapping(value = "/parse")
	public ResponseEntity<UrlModel> postCustomer(@RequestBody UrlToParse url) {
		UrlModel result = null;
		HttpStatus resultCode;
		IParser p = new HtmlParserService();
		try {
			result = p.parseUrl(url.getUrl());
			result.performParsing();
			resultCode = HttpStatus.OK;
		} catch (ParserException e) {
			resultCode  = e.getStatusCode();	
		}
		return new ResponseEntity<UrlModel>(result, resultCode);
	}
}
