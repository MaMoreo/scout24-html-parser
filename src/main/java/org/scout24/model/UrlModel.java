package org.scout24.model;

import java.util.Map;

import org.jsoup.nodes.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UrlModel {

	private Document document;
	private String htmlVersion;
	private String siteTitle;
	private Map<String, Integer> headingsCounter;
	private String baseUri;
	private Map<String, Integer> hypermediaLinks;
	private boolean containsLoginForm;

	/**
	 * Empty constructor.
	 */
	public UrlModel() {
	}

	public UrlModel(Document doc) {
		this.document = doc;
		this.baseUri = doc.baseUri();
	}

	/**
	 * Perform the Parsing of the URL contained in the document.
	 */
	public void performParsing() {
		//FIXME: Concurrencia con hebras ?
		//FIXME: ConcurrenceHashMap para 
		htmlVersion = UrlParser.readHtmlVersion(document);
		siteTitle = document.title();
		headingsCounter = UrlParser.readHeadingsNumber(document);
		hypermediaLinks = UrlParser.readHypermediaLinks(document);
		containsLoginForm = UrlParser.containsLoginForm(document);
	}
	
	// Getters and Setters
	public String getHtmlVersion() {
		return htmlVersion;
	}

	public void setHtmlVersion(String htmlVersion) {
		this.htmlVersion = htmlVersion;
	}

	public String getSiteTitle() {
		return siteTitle;
	}

	public void setSiteTitle(String siteTitle) {
		this.siteTitle = siteTitle;
	}

	public Map<String, Integer> getHeadingsCounter() {
		return headingsCounter;
	}

	public void setHeadingsCounter(Map<String, Integer> headingsCounter) {
		this.headingsCounter = headingsCounter;
	}

	public String getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(String baseUri) {
		this.baseUri = baseUri;
	}

	public Map<String, Integer> getHypermediaLinks() {
		return hypermediaLinks;
	}

	public void setHypermediaLinks(Map<String, Integer> hypermediaLinks) {
		this.hypermediaLinks = hypermediaLinks;
	}

	@JsonIgnore
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public boolean isContainsLoginForm() {
		return containsLoginForm;
	}

	public void setContainsLoginForm(boolean containsLoginForm) {
		this.containsLoginForm = containsLoginForm;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
	        return false;
	    }
	    if (!UrlModel.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final UrlModel other = (UrlModel) obj;
	    if ((this.htmlVersion == null) ? (other.htmlVersion != null) : !this.htmlVersion.equals(other.htmlVersion)) {
	        return false;
	    }
	    if ((this.siteTitle == null) ? (other.siteTitle != null) : !this.siteTitle.equals(other.siteTitle)) {
	        return false;
	    }
	    
	    if ((this.baseUri == null) ? (other.baseUri != null) : !this.baseUri.equals(other.baseUri)) {
	        return false;
	    }
	    
	    if ((this.headingsCounter == null) ? (other.headingsCounter != null) : !this.headingsCounter.equals(other.headingsCounter)) {
	        return false;
	    }
	    
	    if ((this.hypermediaLinks == null) ? (other.hypermediaLinks != null) : !this.hypermediaLinks.equals(other.hypermediaLinks)) {
	        return false;
	    }
	    
	    if ( this.containsLoginForm != other.containsLoginForm){
	    	return false;
	    }
	    return true;
	}
	
	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.baseUri != null ? this.baseUri.hashCode() : 0);
	    hash = 53 * hash + (this.htmlVersion != null ? this.htmlVersion.hashCode() : 0);
	    hash = 53 * hash + (this.siteTitle != null ? this.siteTitle.hashCode() : 0);
	    hash = 53 * hash + (this.hypermediaLinks != null ? this.hypermediaLinks.hashCode() : 0);
	    hash = 53 * hash + (this.headingsCounter != null ? this.headingsCounter.hashCode() : 0);
	    hash = 53 * hash + (this.containsLoginForm ? 17 : 31);  // some random numbers
	    return hash;
	}
}
