package org.scout24.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * Performs the parsing of the different elements in a Document.
 *  
 * @author Mike
 */
public class UrlParser {

	/**
	 * Obtains the HTML version.
	 * 
	 * @return
	 */
	public static String readHtmlVersion(Document  document) {
		List<Node> nodes = document.childNodes();
		// FIXME: needed if we have only one URL ?
		return nodes.stream().filter(node -> node instanceof DocumentType) //
				.map(UrlParser::generateHtmlVersion).collect(Collectors.joining());
	}
	
	/**
	 * Extracts the HTML version from the DOCTYPE.
	 * 
	 * @param node
	 * @return
	 */
	private static String generateHtmlVersion(Node node) {
		String version = "No HTML specified";
		DocumentType documentType = (DocumentType) node;
		String htmlVersion = documentType.attr("publicid");

		if (htmlVersion.contains("DTD")) {
			String substring = htmlVersion.substring(htmlVersion.indexOf("DTD") + 3).trim();
			String regExp = "(\\w|\\s|\\.)*";
			final Pattern p = Pattern.compile(regExp);
			Matcher m = p.matcher(substring);
			if (m.find()) {
				version = m.group(0);
			}
		} else {
			version = "".equals(htmlVersion) ? "HTML 5" : htmlVersion;
		}
		return version;
	}
	
	/**
	 * Retrieves the number of headings grouped by heading level.
	 * 
	 * @return
	 */
	public static Map<String, Integer> readHeadingsNumber(Document document) {
		Map<String, Integer> headingsCounter = new HashMap<>();
		//FIXME usar algo parecido a esto Elements divs = document.select("div");.
		for (int i = 1; i <= 6; i++) {
			Elements elementsByTag = document.getElementsByTag("h" + i);
			headingsCounter.put("h" + i, elementsByTag.size());
		}
		return headingsCounter;
	}
	
	/**
	 * Counts the number of  all the links, grouped in internal(same domain) and external
	 */
	public static Map<String, Integer> readHypermediaLinks(Document document) {
		Map<String, Integer> linksInSite = new HashMap<>();
		linksInSite.put("internal", 0);
		linksInSite.put("external", 0);

		// get all links
		Elements links = document.select("a[href]");
		for (Element link : links) {
			String absUrl = link.absUrl("href");
			if (absUrl.contains(document.baseUri())) {
				// internal link++
				linksInSite.put("internal", linksInSite.get("internal") + 1);
			} else {
				// external link ++
				linksInSite.put("external", linksInSite.get("external") + 1);
			}
		}
		return linksInSite;
	}
	
	/**
	 * Checks if the web site contains a login form.
	 * 
	 * @return true if the web contains a login form, false otherwise
	 */
	public static boolean containsLoginForm(Document  document) {
		Elements divs = document.select("div");
		for (Element element : divs) {
			if (element.attr("id").contains("login") || element.attr("class").contains("login")){ 
				if (element.select("form").size() != 0){
					return true;
				}
			}
		}
		return false;
	}
}
