package org.scout24.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This web application performs the parsing of an HTML web site using the
 * JSoup library. 
 * <br>
 * Use:
 * 		<li> Direct your browser to localhost:8080/</li>
 * 		<li> Introduce an URL </li>
 * 		<li> The server parses the URL and returns an analysis.	</li>
 * 
 * <br><br>
 * Technology:
 * 		<li> View:  
 * 			<ul> 
 * 				<li>Thymeleaf Templates. Using HTML 5 + Ajax.</li>
 * 			</ul> 
 * 		</li>
 * 		<li> Model:  
 * 			<ul> 
 * 					<li> UrlModel: Performs the parsing. </li>
 * 			   		<li> UrlToParse: Communication between the View and the Controller (Form)</li>
 * 			</ul>

 * 		</li>
 * 		<li> Controller:  Two Controllers:
 * 			   <ul> 
 * 					<li>REST Controller to handle the Ajax Call (Form) </li>
 * 			   		<li>Web Controller to handle the index.html presentation </li>
 * 			</ul>
 *    </li>
 * 
 * @author Miguel Moreo
 */

@SpringBootApplication(scanBasePackages = { "org.scout24.controller"}) 
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}