package org.scout24.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.scout24.app.Application;
import org.scout24.model.UrlModel;
import org.scout24.model.UrlToParse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * This Tests performs a tests against a MVC SERVER that is started by Spring.
 * 
 * @author Mike
 *
 */
@RunWith(SpringRunner.class) // SpringRunner is an alias for the
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ParseUrlTest {

	@Value("${local.server.port}")
	private int port;

	 @Test
	public void test() {
		UrlModel expectedResult = new UrlModel();

		expectedResult.setBaseUri("https://github.com/");

		Map<String, Integer> headingMap = new HashMap<>();
		headingMap.put("h1", 8);
		headingMap.put("h2", 5);
		headingMap.put("h3", 6);
		headingMap.put("h4", 6);
		headingMap.put("h5", 0);
		headingMap.put("h6", 4);
		expectedResult.setHeadingsCounter(headingMap);

		expectedResult.setHtmlVersion("HTML 5");

		Map<String, Integer> hypermediaMap = new HashMap<>();
		hypermediaMap.put("internal", 50);
		hypermediaMap.put("external", 16);
		expectedResult.setHypermediaLinks(hypermediaMap);

		expectedResult.setSiteTitle("The world's leading software development platform " + "\u00B7" + " GitHub");

		UrlToParse url = new UrlToParse();
		url.setUrl("https://github.com/");
		
		RestTemplate rest = new RestTemplate();
		UrlModel result = rest.postForObject("http://localhost:{port}/parse", url, UrlModel.class, port);
		assertEquals(expectedResult, result);
	}

	@Test(expected = HttpClientErrorException.class)
	public void pageNotFound() {
		try {
			RestTemplate rest = new RestTemplate();
			rest.getForObject("http://localhost:{port}/bogusPage", String.class, port);
			fail("Should result in HTTP 404");
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
			throw e;
		} 
	}
	
	@Test
	public void pageFound() {
		try {
			
			UrlToParse url = new UrlToParse();
			url.setUrl("https://github.com/");
			
			RestTemplate rest = new RestTemplate();
			String result = rest.postForObject("http://localhost:{port}/parse", url, String.class, port);
			
			//FIXME replace the middle point for the UNI CODE see other examples
			String expectedResult = "{\"htmlVersion\":\"HTML 5\",\"siteTitle\":\"The world\'s leading software development platform � GitHub";
			assertTrue(result.contains(expectedResult));
			
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
			throw e;
		} 
	}
}
