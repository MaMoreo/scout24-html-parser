package org.scout24.integration;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.scout24.app.Application;
import org.scout24.model.UrlToParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This Tests performs a tests against a MVC SERVER that is mocked.
 * 
 * @author Mike
 *
 */
@RunWith(SpringRunner.class)   // SpringRunner is an alias for the SpringJUnit4ClassRunner. 
@SpringBootTest(classes = Application.class)   // where to find the Configuration... among other stuff
@WebAppConfiguration   //Enables web context testing
public class RestWebControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	//***  Spring MVC MOCK and its Web Context
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	// ****
	
	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private ObjectMapper mapper;

	
    // Needed by our App.
	private Map<String, Integer> expectedHeadingsCounter;
	private Map<String, Integer> expectedHypermediaLinks;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	/**
	 * Adds some data to test.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		
		expectedHeadingsCounter = new HashMap<>();
		expectedHeadingsCounter.put("h1", 8);
		expectedHeadingsCounter.put("h2", 5);
		expectedHeadingsCounter.put("h3", 6);
		expectedHeadingsCounter.put("h4", 6);
		expectedHeadingsCounter.put("h5", 0);
		expectedHeadingsCounter.put("h6", 4);
		
		expectedHypermediaLinks = new HashMap<>();
		expectedHypermediaLinks.put("internal", 50);
		expectedHypermediaLinks.put("external", 16);
	}

	/**
	 * Tests that the user can obtain the information from the requested URL.
	 * 
	 * Steps:
	 * <ol>
	 * <li>The client makes a call to the root URL, providing the URL to test</li>
	 * <li>The system returns the parsed URL</li>
	 * </ol>
	 * <br>
	 * Expected: The parsed information matches the expected one.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testParseUrl() throws Exception {
		UrlToParse url = new UrlToParse();
		url.setUrl("https://github.com/");
		String json = mapper.writeValueAsString(url);
		
		mockMvc.perform(post("/parse").contentType(contentType)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("htmlVersion", is("HTML 5")))
				.andExpect(jsonPath("baseUri", is("https://github.com/")))
				.andExpect(jsonPath("siteTitle", is("The world's leading software development platform "+"\u00B7"+" GitHub")))
				.andExpect(jsonPath("headingsCounter", is(expectedHeadingsCounter)))
				.andExpect(jsonPath("hypermediaLinks", is(expectedHypermediaLinks)));
	}
}
