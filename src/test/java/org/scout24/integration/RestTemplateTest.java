package org.scout24.integration;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.scout24.app.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * This Tests performs a tests against a MVC SERVER that is started by Spring.
 * It uses ResponseEntity and restTemplate 
 * 
 * @author Mike
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestTemplateTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void test() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/", String.class);
		assertTrue(HttpStatus.OK.equals(responseEntity.getStatusCode()));
	}

}
