package org.scout24.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.scout24.exception.ParserException;
import org.scout24.model.UrlModel;
import org.scout24.service.HtmlParserService;

public class ParserTest {

	private HtmlParserService parser = new HtmlParserService();
	private UrlModel urlModelMock = mock(UrlModel.class);

	@Before
	public void setUp() throws Exception {
				// define return value for methods
				when(urlModelMock.getBaseUri()).thenReturn("https://github.com/");
				Map<String, Integer> headingMap = new HashMap<>();
				headingMap.put("h1", 8);
				headingMap.put("h2", 5);
				headingMap.put("h3", 6);
				headingMap.put("h4", 6);
				headingMap.put("h5", 0);
				headingMap.put("h6", 4);
				when(urlModelMock.getHeadingsCounter()).thenReturn(headingMap);
				
				when(urlModelMock.getHtmlVersion()).thenReturn("HTML 5");
				
				Map<String, Integer> hypermediaMap = new HashMap<>();
				hypermediaMap.put("internal", 50);
				hypermediaMap.put("external", 16);
				when(urlModelMock.getHypermediaLinks()).thenReturn(hypermediaMap);
				
				when(urlModelMock.getSiteTitle()).thenReturn("The world's leading software development platform "+"\u00B7"+" GitHub");
	}

	@Test
	public void testParseUrl() throws IOException, ParserException {
		UrlModel urlModel = parser.parseUrl("https://github.com/");
		urlModel.performParsing();
		assertEquals(urlModelMock.getBaseUri(), urlModel.getBaseUri());
		assertEquals(urlModelMock.getHeadingsCounter(), urlModel.getHeadingsCounter());
		assertEquals(urlModelMock.getHtmlVersion(), urlModel.getHtmlVersion());
		assertEquals(urlModelMock.getHypermediaLinks(), urlModel.getHypermediaLinks());
		assertEquals(urlModelMock.getSiteTitle(), urlModel.getSiteTitle());
	}
	
	/**
	 * Verifies the exception is thrown when the URL is invalid: "htt" instead of "http"
	 * @throws ParserException
	 */
	@Test(expected = ParserException.class)
	public void testParserUrlException() throws ParserException {
		verify(parser.parseUrl("htt://github.com/"));
	}
}
