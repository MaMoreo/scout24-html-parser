package org.scout24.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.scout24.model.UrlParser;

public class UrlParserTest {
	
	private Document document;
	private Document document2;

	@Before
	public void setUp() throws Exception {
		File input = new File("src/test/resources/html/example.html");
		document = Jsoup.parse(input, "UTF-8", "");

		File input2 = new File("src/test/resources/html/example5.html");
		document2 = Jsoup.parse(input2, "UTF-8", "");
	}

	@Test
	public void testReadHtmlVersion() {
		String htmlVersion = UrlParser.readHtmlVersion(document);
		assertEquals(htmlVersion, "HTML 4.01 Transitional");
	}
	
	@Test
	public void testReadtHtmlVersion5() throws IOException {
		String htmlVersion =  UrlParser.readHtmlVersion(document2);
		assertEquals(htmlVersion, "HTML 5");
	}
	
	@Test
	public void testgetHeadingsNumber() {
		Map<String, Integer> map = new HashMap<>();
		map.put("h1", 2);
		map.put("h2", 1);
		map.put("h3", 0);
		map.put("h4", 0);
		map.put("h5", 1);
		map.put("h6", 0);

		Map<String, Integer> headingsNumber = UrlParser.readHeadingsNumber(document);
		assertEquals(map, headingsNumber);
	}
	
	@Test
	public void testContainsLoginForm() throws IOException{
		document = Jsoup.connect("https://www.spiegel.de/meinspiegel/login.html").get();
		assertTrue(UrlParser.containsLoginForm(document));
		
		document = Jsoup.connect("https://github.com/login").get();
		assertTrue(UrlParser.containsLoginForm(document));
		
		document = Jsoup.connect("https://jsoup.org/cookbook/extracting-data/example-list-links").get();
		assertFalse(UrlParser.containsLoginForm(document));
	}
	
	@Test
	public void testgetHypermediaLinksFile() {
		Map<String, Integer> map = new HashMap<>();
		map.put("internal", 1);
		map.put("external", 1);
		document.setBaseUri("http://example.com/");

		Map<String, Integer> links = UrlParser.readHypermediaLinks(document);
		assertEquals(map, links);
	}

	@Test
	public void testgetHypermediaLinksWeb() throws IOException {
		Map<String, Integer> map = new HashMap<>();
		map.put("internal", 50);
		map.put("external", 16);

		document = Jsoup.connect("https://github.com/").get();

		Map<String, Integer> links = UrlParser.readHypermediaLinks(document);
		assertEquals(map, links);
	}

}
