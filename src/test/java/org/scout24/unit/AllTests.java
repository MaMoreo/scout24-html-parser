package org.scout24.unit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ParserTest.class, UrlModelTest.class, UrlParserTest.class })
public class AllTests {

}
