# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Creates a web application that parsers HTML files. 
* Code Challenge!
* 0.1.0

### How do I get set up? ###
No setup is needed for this app.


### HOW TO RUN THE APP ###

There are two ways to start the application: 
	1) run it from the command line using Gradle
	2) create a JAR file an run it.

1) Run the application from the command line with Gradle. 
  >./gradlew bootRun. 

2) Build a single executable JAR file that contains all the necessary dependencies, classes, and resources. 
  >./gradlew build. 

Run the JAR file:
> java -jar build/libs/scout24-html-parser-0.1.0.jar

Open a browser and go to http://localhost:8080/
The main site should appear.

### DESIGN ###

The structure is as follows:
	The main folder contains:
		- App 		: To make the application runnable.
		- Controllers	: ErrorController, WebController (to show the index page), RestController.
		- Service	: The Service that uses the Model
		- Exceptions	: Object to handle the exceptions.
		- Model		: UrlToParse (Object to transfer the info to the REST Controller)
				  UrlModel (Response of the Controller)
		- Resources	: HTML, js Files
        The test folder with
		- Unit Tests
		- Functional Test
		- Resources	: HTML to test

Algorithm to check the existance of a login Form:
	I am just checking if there is a div with the id or class named "login"
	if I am able to find it, I will check if there is a Form inside. This
	seems to be quite inneficcient and maybe a better approach should be considered

Test Provided:
	Unit Tests
	Functional Tests


### Dependencies ###
* JSoup - 

### Database configuration ###
* No Database is used for this application.

### How to run tests ###
* There is a test suite to run the unit tests.
* Functional Test can be run using Junit.

### ENHANCEMENTS ###

- Provide more tests:
	Integration Tests	
	Sanity Tests
- Use Log classes during the application
- Better Exception Handling
- Enhancement of the UI

### Who do I talk to? ###

* miguelangel.moreo@gmail.com
